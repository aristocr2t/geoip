#!/bin/bash

cd $(dirname $(readlink -f "$0"));

APP_NAME='test_backend';
APP_IMAGE='test_backend';
POSTGRES_NAME='test_postgres';
RABBITMQ_NAME='test_rabbitmq';
REDIS_NAME='test_redis';

bash_ts() {
  date +'%Y-%m-%d %H:%M:%S.%3N';
}

if ! command -v docker >/dev/null; then
  echo -e "\e[36m$(bash_ts)\e[39m install docker";
  curl -fsSL https://get.docker.com -o get-docker.sh;
  sh get-docker.sh >/dev/null;
  sudo groupadd docker;
  sudo usermod -aG docker $USER;
  docker --version;
  rm get-docker.sh;
fi

if ! command -v docker-compose >/dev/null; then
  echo -e "\e[36m$(bash_ts)\e[39m install docker-compose";
  sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose;
  sudo chmod +x /usr/local/bin/docker-compose;
  sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose;
  docker-compose --version;
fi

if [ ! "$(docker ps -q -f name=$POSTGRES_NAME)" ]; then
  if [ "$(docker ps -aq -f status=exited -f name=$POSTGRES_NAME)" ] ; then
    docker rm $POSTGRES_NAME >/dev/null;
  fi
  echo -e "\e[36m$(bash_ts)\e[39m install $POSTGRES_NAME";
  docker-compose up --detach --build $POSTGRES_NAME >/dev/null;
fi

if [ ! "$(docker ps -q -f name=$RABBITMQ_NAME)" ]; then
  if [ "$(docker ps -aq -f status=exited -f name=$RABBITMQ_NAME)" ] ; then
    docker rm $RABBITMQ_NAME >/dev/null;
  fi
  echo -e "\e[36m$(bash_ts)\e[39m install $RABBITMQ_NAME";
  docker-compose up --detach --build $RABBITMQ_NAME >/dev/null;
fi

if [ ! "$(docker ps -q -f name=$REDIS_NAME)" ]; then
  if [ "$(docker ps -aq -f status=exited -f name=$REDIS_NAME)" ] ; then
    docker rm $REDIS_NAME >/dev/null;
  fi
  echo -e "\e[36m$(bash_ts)\e[39m install $REDIS_NAME";
  docker-compose up --detach --build $REDIS_NAME >/dev/null;
fi

if [ "$(docker ps -aq -f name=$APP_NAME)" ]; then
  echo -e "\e[36m$(bash_ts)\e[39m remove old $APP_NAME";
  docker stop $APP_NAME;
  docker rm $APP_NAME;
  echo -e "\e[36m$(bash_ts)\e[39m remove old $APP_NAME image $APP_IMAGE";
  docker rmi $APP_IMAGE;
fi

echo -e "\e[36m$(bash_ts)\e[39m install $APP_NAME";
docker-compose up --detach --build $APP_NAME;

if [ "$(docker ps -q -f name=$APP_NAME)" ]; then
  docker exec -it $APP_NAME /bin/sh -c "npm run migrate:prod";
  docker exec -it $APP_NAME /bin/sh -c "npm run seed:prod";
fi
