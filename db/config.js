const { existsSync, readFileSync } = require('fs');
const { resolve: pathResolve } = require('path');

const default_ = getConfig('../src/configs/default.json');
const development = getConfig('../src/configs/development.json');
const test = getConfig('../src/configs/test.json');
const env = getEnv('../.env');

const production = env
	? {
			...default_,
			host: env.POSTGRES_HOST,
			port: env.POSTGRES_PORT,
			database: env.POSTGRES_DB,
			username: env.POSTGRES_USER,
			password: env.POSTGRES_PASSWORD,
			migrationStorage: 'sequelize',
			migrationStorageTableName: '_migrations',
			seederStorage: 'sequelize',
			seederStorageTableName: '_seeders',
	  }
	: null;

module.exports = {
	test: {
		...default_,
		...test,
		migrationStorage: 'sequelize',
		migrationStorageTableName: '_migrations',
		seederStorage: 'none',
	},
	development: {
		...default_,
		...development,
		migrationStorage: 'sequelize',
		migrationStorageTableName: '_migrations',
		seederStorage: 'sequelize',
		seederStorageTableName: '_seeders',
	},
	production,
};

function getEnv(filename) {
	const filepath = pathResolve(__dirname, filename);
	return existsSync(filepath)
		? readFileSync(filepath, { encoding: 'utf8' })
				.split(/\r?\n/)
				.filter(v => /^[\w\d]+=/.test(v))
				.map(s => {
					const [m, k, v] = s.match(/^([\w\d]+)=(.*)/);
					return { [k]: v };
				})
				.reduce((p, n) => Object.assign(p, n), {})
		: {};
}

function getConfig(filename) {
	try {
		return { ...(require(filename).postgres || {}) };
	} catch {
		return {};
	}
}
