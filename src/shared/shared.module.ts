import { HttpModule, Module } from '@nestjs/common';

import { SocketService } from './services/socket/socket.service';

@Module({
	imports: [HttpModule],
	exports: [
		//
		HttpModule,
		SocketService,
	],
	providers: [
		//
		SocketService,
	],
})
export class SharedModule {
	private static handlersStarted = false;

	constructor() {
		this.runHandlers();
	}

	private runHandlers(): void {
		if (SharedModule.handlersStarted) {
			return;
		}

		SharedModule.handlersStarted = true;
	}
}
