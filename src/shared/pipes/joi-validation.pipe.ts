import Joi, { SchemaMap } from '@hapi/joi';
import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform, Type } from '@nestjs/common';

const VALIDATION_TYPES: { [key: string]: string } = {
	param: 'VALIDATION:PARAM_SCHEMA',
	query: 'VALIDATION:QUERY_SCHEMA',
	body: 'VALIDATION:BODY_SCHEMA',
};

@Injectable()
export class JoiValidationPipe implements PipeTransform {
	transform<T>(value: T, metadata: ArgumentMetadata): T {
		const assembledSchema: SchemaMap = {};

		let schema: SchemaMap,
			metatype: Type<any> | undefined = metadata.metatype;

		const schemas: SchemaMap[] = [];

		do {
			schema = metatype ? Reflect.getMetadata(VALIDATION_TYPES[metadata.type], metatype) : null;

			if (!schema) {
				break;
			}

			schemas.push(schema);

			metatype = Object.getPrototypeOf(metatype);
		} while (schema);

		Object.assign(assembledSchema, ...schemas.reverse());

		if (schemas.length > 0) {
			const { value: result, error } = Joi.object(assembledSchema)
				.options({ stripUnknown: true })
				.validate(value);

			if (error) {
				throw new BadRequestException('Неверно заданы параметры', error as any);
			}

			return result;
		}

		return value;
	}
}
