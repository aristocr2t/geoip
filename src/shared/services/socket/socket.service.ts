import socketIo from 'socket.io';
import redis from 'socket.io-redis';
import { Injectable } from '@nestjs/common';
import { isObject, isString } from 'util';

import config from '../../../config';
import { WebSocketCluster } from '../../../utils';

@Injectable()
export class SocketService {
	private static clients: { [tokenId: string]: WebSocketCluster.Socket } = {};
	private static tokens: { [clientId: string]: any } = {};
	private static io: socketIo.Server;
	private static ioCluster: WebSocketCluster.Server;

	static checkSocket(clientId: string): [any, WebSocketCluster.Socket] | null {
		const token = SocketService.tokens[clientId];
		if (token) {
			const client = SocketService.clients[token.id];
			if (client?.isOwn) {
				return [token, client];
			}
		}
		return null;
	}

	static runWebSocketCluster(options: SocketIO.ServerOptions): WebSocketCluster.Server {
		this.io = socketIo(options);
		this.io.adapter(redis(config.redis));
		this.ioCluster = new WebSocketCluster.Server(this.io);

		this.ioCluster.handleConnect(client => {
			if (client.isOwn) {
				client.emit('connection', { statusCode: 200, message: 'OK' });
			}

			client.on('auth/login', async (data: { accessKey: string }) => {
				if (!isObject(data) || !isString(data.accessKey)) {
					return;
				}

				const token = await this.checkAuth(data.accessKey);

				if (!token) {
					return;
				}

				this.tokens[client.id] = token;
				this.clients[token.getDataValue('id')] = client;
			});
		});

		this.ioCluster.handleDisconnect(client => {
			const token = this.tokens[client.id];
			if (token) {
				delete this.tokens[client.id];
				delete this.clients[token.getDataValue('id')];
			}
		});
		return this.ioCluster;
	}

	static async checkAuth(accessKey: string): Promise<any | null> {
		return null;
	}

	constructor() {}
}
