import XRegExp from 'xregexp';
import Joi, { SchemaMap } from '@hapi/joi';

export type GetListQuery<T extends {} = any> = {
	pageIndex?: number;
	pageSize?: number;
	orderBy?: string;
	direction?: 'asc' | 'desc';
	searchText?: string;
} & T;

export function ParamSchema<T>(schema: SchemaMap<T>): (target: new () => Partial<T>) => void {
	return Reflect.metadata('VALIDATION:PARAM_SCHEMA', schema);
}

export function QuerySchema<T>(schema: SchemaMap<T>): (target: new () => Partial<T>) => void {
	return Reflect.metadata('VALIDATION:QUERY_SCHEMA', schema);
}

export function BodySchema<T>(schema: SchemaMap<T>): (target: new () => Partial<T>) => void {
	return Reflect.metadata('VALIDATION:BODY_SCHEMA', schema);
}

const _domains = {
	any: (required: boolean = false) => (required ? Joi.any().required() : Joi.any().default(null)),
	authorization: (required: boolean = false) =>
		required
			? Joi.string()
					.regex(/^Bearer [\w\d]{64}$/)
					.required()
			: Joi.string()
					.regex(/^$|^Bearer [\w\d]{64}$/)
					.allow('', null)
					.empty(['', null])
					.default(''),
	accessKey: (required: boolean = false) =>
		required
			? Joi.string()
					.length(64)
					.required()
			: Joi.string()
					.length(64)
					.allow('', null)
					.empty(['', null])
					.default(null),
	id: (required: boolean = false) =>
		required
			? Joi.string()
					.uuid({ version: ['uuidv4'] })
					.required()
			: Joi.string()
					.uuid({ version: ['uuidv4'] })
					.allow('', null)
					.empty(['', null])
					.default(null),
	ids: (required: boolean = false) =>
		required
			? Joi.array()
					.required()
					.min(1)
					.max(100)
					.items(Joi.string().uuid({ version: ['uuidv4'] }))
			: Joi.array()
					.allow(null)
					.empty([null])
					.default([])
					.max(100)
					.items(Joi.string().uuid({ version: ['uuidv4'] })),
	integer: (required: boolean = false, defaultValue: number = 0) =>
		required
			? Joi.number()
					.integer()
					.required()
			: Joi.number()
					.integer()
					.allow(null)
					.empty(['', null])
					.default(defaultValue),
	real: (required: boolean = false, defaultValue: number = 0) =>
		required
			? Joi.number().required()
			: Joi.number()
					.allow(null)
					.empty(['', null])
					.default(defaultValue),
	login: (required: boolean = false) =>
		required
			? Joi.alternatives()
					.try(
						Joi.string()
							.replace(/([^\+\d]+)/g, '')
							.regex(/^\+\d{11,13}$/),
						Joi.string()
							.trim()
							.email()
							.lowercase(),
					)
					.required()
			: Joi.alternatives()
					.try(
						Joi.string()
							.replace(/([^\+\d]+)/g, '')
							.regex(/^\+\d{11,13}$/),
						Joi.string()
							.trim()
							.email()
							.lowercase(),
					)
					.allow('', null)
					.empty(['', null])
					.default(null),
	password: (required: boolean = false) =>
		required
			? Joi.string()
					.min(6)
					.max(32)
					.required()
			: Joi.string()
					.min(6)
					.max(32)
					.allow('', null)
					.empty(['', null])
					.default(null),
	phone: (required: boolean = false) =>
		required
			? Joi.string()
					.replace(/([^\+\d]+)/g, '')
					.regex(/^\+\d{11,13}$/)
					.required()
			: Joi.string()
					.replace(/([^\+\d]+)/g, '')
					.regex(/^\+\d{11,13}$/)
					.allow('', null)
					.empty(['', null])
					.default(null),
	email: (required: boolean = false) =>
		required
			? Joi.string()
					.trim()
					.email()
					.lowercase()
					.required()
			: Joi.string()
					.trim()
					.email()
					.lowercase()
					.allow('', null)
					.empty(['', null])
					.default(null),
	uname: (required: boolean = false) =>
		required
			? Joi.string()
					.lowercase()
					.trim()
					.regex(/^([a-z\d_]+\.)*[a-z\d_]+$/)
					.min(5)
					.max(64)
					.required()
			: Joi.string()
					.lowercase()
					.trim()
					.regex(/^([a-z\d_]+\.)*[a-z\d_]+$/)
					.min(5)
					.max(64)
					.allow('', null)
					.empty(['', null])
					.default(null),
	website: (required: boolean = false) =>
		required
			? Joi.string()
					.trim()
					.lowercase()
					.regex(XRegExp('^(?:https?://)?(?:[\\p{L}\\d-]+\\.){1,3}(?:[a-z]{2,6})(?:/.*)?'))
					.replace(XRegExp('^(?:https?://)?((?:[\\p{L}\\d-]+\\.){1,3}(?:[a-z]{2,6}))(?:/.*)?'), '$1')
					.required()
			: Joi.string()
					.trim()
					.lowercase()
					.regex(XRegExp('^(?:https?://)?(?:[\\p{L}\\d-]+\\.){1,3}(?:[a-z]{2,6})(?:/.*)?'))
					.replace(XRegExp('^(?:https?://)?((?:[\\p{L}\\d-]+\\.){1,3}(?:[a-z]{2,6}))(?:/.*)?'), '$1')
					.allow('', null)
					.empty(['', null])
					.default(''),
	url: (required: boolean = false) =>
		required
			? Joi.string()
					.trim()
					.uri()
					.required()
			: Joi.string()
					.trim()
					.uri()
					.allow('', null)
					.empty(['', null])
					.default(''),
	coords: (required: boolean = false) =>
		required
			? Joi.array()
					.items(
						Joi.number()
							.min(-90)
							.max(90)
							.required()
							.custom(value => +(+value).toFixed(5)),
						Joi.number()
							.min(-180)
							.max(180)
							.required()
							.custom(value => +(+value).toFixed(5)),
					)
					.length(2)
					.required()
			: Joi.array()
					.items(
						Joi.number()
							.min(-90)
							.max(90)
							.required()
							.custom(value => +(+value).toFixed(5)),
						Joi.number()
							.min(-180)
							.max(180)
							.required()
							.custom(value => +(+value).toFixed(5)),
					)
					.length(2)
					.allow(null)
					.empty(['', null])
					.default(null),
	enum: <K = string>(only: K[], required: boolean = false) =>
		required
			? Joi.string()
					.max(63)
					.valid(...only)
					.required()
			: Joi.string()
					.max(63)
					.valid(...only)
					.allow('', null)
					.empty(['', null])
					.default(null),
	enums: <K = string>(only: K[], required: boolean = false) =>
		required
			? Joi.array()
					.required()
					.max(100)
					.items(
						Joi.string()
							.max(32)
							.valid(...only),
					)
					.unique()
			: Joi.array()
					.allow(null)
					.empty([null])
					.default([])
					.max(100)
					.items(
						Joi.string()
							.max(32)
							.valid(...only),
					)
					.unique(),
	string: (required: boolean = false, max: number = 255, min: number = 0) =>
		required
			? Joi.string()
					.max(max)
					.min(min)
					.required()
			: Joi.string()
					.max(max)
					.min(min)
					.allow('', null)
					.empty(['', null])
					.default(''),
	text: (required: boolean = false, defaultValue: string = '') =>
		required
			? Joi.string().required()
			: Joi.string()
					.allow('', null)
					.empty(['', null])
					.default(defaultValue),
	boolean: (required: boolean = false, defaultValue: boolean | null = null) =>
		required
			? Joi.boolean()
					.truthy('1', 'y', 'Y', 'true')
					.falsy('0', 'n', 'N', 'false')
					.required()
			: Joi.boolean()
					.truthy('1', 'y', 'Y', 'true')
					.falsy('0', 'n', 'N', 'false')
					.allow('', null)
					.empty(['', null])
					.default(defaultValue),
	date: (required: boolean = false) =>
		required
			? Joi.date()
					.iso()
					.required()
			: Joi.date()
					.iso()
					.allow(null)
					.empty(['', null])
					.default(null),
	array: (items: Joi.Schema[], required: boolean = false) =>
		required
			? Joi.array()
					.required()
					.items(...items)
			: Joi.array()
					.allow(null)
					.empty([null])
					.default([])
					.items(...items),
	object: <T extends {}>(schema?: SchemaMap<T>, required: boolean = false, defaultValue: object = {}) =>
		required
			? Joi.object(schema || {})
					.options({ stripUnknown: true })
					.required()
			: Joi.object(schema || {})
					.options({ stripUnknown: true })
					.allow({}, null)
					.empty([{}, null])
					.default(defaultValue),
};

export const DOMAINS = new Proxy(_domains, { get: <T, P extends keyof T>(target: T, prop: P) => target[prop] });
