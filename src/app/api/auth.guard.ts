import { CanActivate, ExecutionContext, Injectable, SetMetadata } from '@nestjs/common';

export const ROLES_TOKEN = 'roles';
export const NON_STRICT_TOKEN = 'non-strict';

export const Roles = (...roles: string[]) => SetMetadata(ROLES_TOKEN, roles);
export const IsNonStrict = () => SetMetadata(NON_STRICT_TOKEN, true);

@Injectable()
export class AuthGuard implements CanActivate {
	constructor() {}

	async canActivate(context: ExecutionContext): Promise<boolean> {
		return true;
	}
}
