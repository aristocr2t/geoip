import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';

import { requestLogger } from '../../utils';

@Catch()
export class ApiFilter implements ExceptionFilter {
	catch(err: any, host: ArgumentsHost) {
		const ctx = host.switchToHttp();
		const request = ctx.getRequest<FastifyRequest>();
		const reply = ctx.getResponse<FastifyReply>();

		let message: string | object;

		if (err instanceof HttpException) {
			message = err.getResponse();
		} else {
			const url = request.raw.url;
			const ip = request.headers['x-forwarded-for'] || request.headers['x-real-ip'] || request.ip;
			const userAgent = request.headers['user-agent'];

			err.message += ` in ${url} ${ip} ${userAgent}`;

			message = {
				statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
				message: 'Внутренняя ошибка сервера',
				error: err.message,
			};

			requestLogger.error(err);
		}

		const statusCode = (message as { statusCode: number })?.statusCode || HttpStatus.OK;

		reply.status(statusCode).send(message);
	}
}
