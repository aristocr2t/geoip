import cacheManager from 'cache-manager';
import redisStore from 'cache-manager-redis-store';
import { CACHE_MANAGER, Module } from '@nestjs/common';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';

import config from '../../config';
import { SharedModule } from '../../shared/shared.module';
import { ApiFilter } from './api.filter';
import { ApiInterceptor } from './api.interceptor';
import { JoiValidationPipe } from '../../shared/pipes/joi-validation.pipe';
import { AuthGuard } from './auth.guard';
import { GeoipController } from './geoip/geoip.controller';
import { GeoipService } from './geoip/geoip.service';

@Module({
	imports: [SharedModule],
	controllers: [GeoipController],
	providers: [
		{ provide: CACHE_MANAGER, useValue: cacheManager.caching({ store: redisStore, ...config.redis, ...config.cacheManager }) },
		{ provide: APP_INTERCEPTOR, useClass: ApiInterceptor },
		{ provide: APP_FILTER, useClass: ApiFilter },
		{ provide: APP_PIPE, useClass: JoiValidationPipe },
		{ provide: APP_GUARD, useClass: AuthGuard },
		GeoipService,
	],
})
export class ApiModule {
	constructor() {}
}
