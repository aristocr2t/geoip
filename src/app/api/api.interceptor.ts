import { CacheInterceptor, CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { CACHE_KEY_METADATA, CACHE_TTL_METADATA } from '@nestjs/common/cache/cache.constants';
import { FastifyAdapter } from '@nestjs/platform-fastify';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { EmptyResponseData } from '../../typings';

@Injectable()
export class ApiInterceptor extends CacheInterceptor implements NestInterceptor {
	private remapping(data: any) {
		return Object.assign({ statusCode: 200, message: null }, data || {});
	}

	async intercept<T = EmptyResponseData>(context: ExecutionContext, next: CallHandler<T>): Promise<Observable<T>> {
		const key = this.trackBy(context);

		if (!key) {
			return next.handle().pipe(map(this.remapping));
		}

		try {
			const value = await this.cacheManager.get(key);

			if (value) {
				return of(value);
			}

			const ttl = this.reflector.get(CACHE_TTL_METADATA, context.getHandler()) || null;

			return next.handle().pipe(
				map(this.remapping),
				tap(response => {
					const args = ttl ? [key, response, { ttl }] : [key, response];
					this.cacheManager.set.apply(this.cacheManager, args);
				}),
			);
		} catch {
			return next.handle().pipe(map(this.remapping));
		}
	}

	trackBy(context: ExecutionContext): string | undefined {
		const httpAdapter = this.httpAdapterHost.httpAdapter as FastifyAdapter;
		const isHttpApp = httpAdapter && !!httpAdapter.getRequestMethod;
		const cacheMetadata = this.reflector.get(CACHE_KEY_METADATA, context.getHandler());

		if (!isHttpApp || cacheMetadata) {
			return cacheMetadata;
		}

		const request = context.getArgByIndex<FastifyRequest>(0);

		if (httpAdapter.getRequestMethod(request) !== 'GET') {
			return undefined;
		}

		return httpAdapter.getRequestUrl(request);
	}
}
