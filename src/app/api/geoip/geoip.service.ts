import { HttpService, Injectable } from '@nestjs/common';

@Injectable()
export class GeoipService {
	constructor(private readonly $http: HttpService) {}

	get(ip: string): Promise<FreegeoipResponse> {
		return this.$http
			.get<FreegeoipResponse>(`https://freegeoip.app/json/${ip}`)
			.toPromise()
			.then(res => res.data);
	}
}

export interface FreegeoipResponse {
	ip: string;
	country_code: string;
	country_name: string;
	region_code: string;
	region_name: string;
	city: string;
	zip_code: string;
	time_zone: string;
	latitude: number;
	longitude: number;
	metro_code: number;
}
