import { Controller, Get, Req } from '@nestjs/common';

import { FreegeoipResponse, GeoipService } from './geoip.service';
import { ResponseData } from '../../../typings';

@Controller('api/geoip')
export class GeoipController {
	constructor(private readonly $geoip: GeoipService) {}

	@Get()
	async get(@Req() req: FastifyRequest): Promise<ResponseData<FreegeoipResponse>> {
		const ip = req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.ip;
		const item = await this.$geoip.get(ip);
		return { item };
	}
}
