import { DOMAINS, GetListQuery, ParamSchema, QuerySchema } from '../../shared';
import { ObjectId } from '../../typings';

@ParamSchema({
	id: DOMAINS.id(true),
})
export class ParamPdo {
	id!: ObjectId;
}

@QuerySchema({
	pageIndex: DOMAINS.integer()
		.min(0)
		.default(0),
	pageSize: DOMAINS.integer()
		.min(-1)
		.default(10),
	orderBy: DOMAINS.string(),
	direction: DOMAINS.enum(['asc', 'desc']).default('asc'),
	searchText: DOMAINS.string(),
})
export class ListQueryPdo implements GetListQuery {
	pageIndex!: number;
	pageSize!: number;
	orderBy!: string;
	direction!: 'asc' | 'desc';
	searchText!: string;
}
