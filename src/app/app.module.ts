import { Module } from '@nestjs/common';

import { SharedModule } from '../shared/shared.module';
import { ApiModule } from './api/api.module';

@Module({
	imports: [ApiModule, SharedModule],
	controllers: [],
	providers: [],
})
export class AppModule {
	constructor() {}
}
