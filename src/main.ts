import './override-console';
import './typings';

import cluster from 'cluster';
import fmp from 'fastify-multipart';
import os from 'os';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { join } from 'path';

import config from './config';
import { AppModule } from './app/app.module';
import { startForking } from './cluster';
import { databaseSync } from './database';
import { SocketService } from './shared';
import { logger, reqLogger } from './utils';

const workersCount = process.env.NODE_ENV === 'production' ? os.cpus().length : 0;
const promise: Promise<any> = Promise.resolve().then(() => databaseSync);

if (cluster.isMaster && workersCount > 0) {
	promise.then(startForking(workersCount)).catch(logger.error);
} else {
	const { port, host } = config.http;
	promise
		.then(() => bootstrap())
		.catch(logger.error)
		.then(app => (app ? app.listen(port, host) : null))
		.catch(logger.error)
		.then(() => logger.info(`ready on http://${host || 'localhost'}:${port}`));
}

async function bootstrap(): Promise<NestFastifyApplication> {
	const adapter = new FastifyAdapter({ logger: false });

	adapter.register(fmp, { limits: {} });

	const app = await NestFactory.create<NestFastifyApplication>(AppModule, adapter, { logger: false });

	app.use(reqLogger);

	app.enableCors({
		origin: true,
		methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
		allowedHeaders: ['Authorization', 'Content-Type'],
	});

	app.useStaticAssets({
		root: join(__dirname, '../public'),
		prefix: '/',
	});

	app.setViewEngine({
		engine: { ejs: require('ejs') },
		templates: join(__dirname, '../views'),
	});

	SocketService.runWebSocketCluster({
		path: '/sockets',
		transports: ['websocket'],
	});
	return app;
}
