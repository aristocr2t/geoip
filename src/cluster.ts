import cluster from 'cluster';
import { isArray, isFunction, isNumber, isObject, isString } from 'util';

export function startForking(workersCount: number): () => void {
	return () => {
		console.info(`spawn ${workersCount} workers`);
		for (let i = 0; i < workersCount; i++) {
			cluster.fork();
		}
	};
}

const ALWAYS_RESTART = true;

if (cluster.isMaster) {
	function messageHandler(message: { pid: number; event: string; args: any[] }) {
		const otherWorkers = Object.keys(cluster.workers)
			// @ts-ignore
			.filter(id => id !== `${this.id}`)
			.map(id => cluster.workers[id])
			.filter((x): x is cluster.Worker => !!x);
		for (let i = 0, length = otherWorkers.length; i < length; i++) {
			otherWorkers[i].send(message);
		}
	}

	cluster.on('listening', (worker, { address, port }) => {
		console.info(`worker ${worker.id} connected to ${address || 'localhost'}:${port}`);
	});

	cluster.on('fork', worker => {
		console.info(`worker ${worker.id} spawned`);
		worker.on('message', messageHandler.bind(worker));
	});

	cluster.on('online', worker => {
		console.info(`worker ${worker.id} online`);
	});

	cluster.on('disconnect', worker => {
		console.info(`worker ${worker.id} disconnected`);
	});

	cluster.on('exit', (worker, code, signal) => {
		console.info(`worker ${worker.id} died (${signal ?? code})`);
		if (((worker.process as any).exitCode as number) !== 0 || ALWAYS_RESTART) {
			console.info('restarting worker');
			cluster.fork();
		}
	});

	process.emitTo = function(): void {};
	process.subscribeTo = function(): void {};
	process.unsubscribeFrom = function(): void {};
} else {
	process._onMessageCallbacks = {} as Record<string, Array<(...args: any[]) => any>>;

	process.on('message', (message: { pid: number; event: string; args: any[] }) => {
		if (
			isObject(message) &&
			isNumber(message.pid) &&
			isString(message.event) &&
			isArray(process._onMessageCallbacks[message.event]) &&
			isArray(message.args)
		) {
			for (let i = 0, length = process._onMessageCallbacks[message.event].length; i < length; i++) {
				process._onMessageCallbacks[message.event][i](...message.args);
			}
		}
	});

	process.emitTo = function(event: string, ...args: any[]): void {
		if (this.send) {
			this.send({ pid: this.pid, event, args });
		}
	};

	process.subscribeTo = function(event: string, cb: (...args: any[]) => any): void {
		if (isFunction(cb)) {
			if (!isArray(this._onMessageCallbacks[event])) {
				this._onMessageCallbacks[event] = [];
			}

			this._onMessageCallbacks[event].push(cb);
		}
	};

	process.unsubscribeFrom = function(event: string, cb?: (...args: any[]) => any): void {
		if (typeof cb === 'function' && isArray(this._onMessageCallbacks[event])) {
			const index = this._onMessageCallbacks[event].indexOf(cb);
			if (~index) {
				this._onMessageCallbacks[event].splice(index, 1);
			}
		} else {
			this._onMessageCallbacks[event] = [];
		}
	};
}
