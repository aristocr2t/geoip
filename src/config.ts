import Configuration from 'merge-config';
import { existsSync } from 'fs';
import { isFunction } from 'util';

import { ConfigInterface } from './typings';
import { getEnv, logger } from './utils';

const config = new Configuration();

const configs: { [key: string]: string } = {
	default: '',
	development: '',
	test: '',
};

const ENV_FILEPATH = '.env';

Object.keys(configs).map(key => (configs[key] = `src/configs/${key}.json`));

config.file(configs.default);

if (process.env.NODE_ENV === 'production' && existsSync(ENV_FILEPATH)) {
	const env = getEnv(ENV_FILEPATH);
	config.set('postgres.host', env.POSTGRES_HOST);
	config.set('postgres.port', env.POSTGRES_PORT);
	config.set('postgres.database', env.POSTGRES_DB);
	config.set('postgres.username', env.POSTGRES_USER);
	config.set('postgres.password', env.POSTGRES_PASSWORD);
	config.set('rabbitmq.host', env.RABBITMQ_HOST);
	config.set('rabbitmq.port', env.RABBITMQ_PORT);
	config.set('rabbitmq.vhost', env.RABBITMQ_DEFAULT_VHOST);
	config.set('rabbitmq.username', env.RABBITMQ_DEFAULT_USER);
	config.set('rabbitmq.password', env.RABBITMQ_DEFAULT_PASS);
	config.set('redis.host', env.REDIS_HOST);
	config.set('redis.port', env.REDIS_PORT);
}

if (process.env.NODE_ENV === 'test' && existsSync(configs.test)) {
	config.file(configs.test);
	logger.info('starting in test mode');
} else if (process.env.NODE_ENV !== 'production' && existsSync(configs.development)) {
	config.file(configs.development);
	process.env.NODE_ENV = 'development';
	logger.info('starting in development mode');
} else {
	process.env.NODE_ENV = 'production';
	logger.warn('starting in production mode');
}

export function editConfig(callbackFn: (config: any) => void) {
	if (isFunction(callbackFn)) {
		callbackFn(config);
	}
}

export default new Proxy(config.get() as ConfigInterface, {
	get: <T, P extends keyof T>(target: T, prop: P) => target[prop],
});

export const sharedConfig = new Proxy((config.get() as ConfigInterface).modules.shared, {
	get: <T, P extends keyof T>(target: T, prop: P) => target[prop],
});
