import { Rabbit } from 'rabbit-queue';

import config from './config';
import { makeDbUrl } from './utils';

const rabbit = new Rabbit(makeDbUrl(config.rabbitmq), { scheduledPublish: true });

export default rabbit;
