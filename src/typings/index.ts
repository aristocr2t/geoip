export * from './config';
export * from './models';
export * from './request';
export * from './response';

declare global {
	namespace NodeJS {
		interface Process {
			_onMessageCallbacks: Record<string, Array<(...args: any[]) => any>>;
			emitTo(event: string, ...args: any[]): void;
			subscribeTo(event: string, cb: (...args: any[]) => any): void;
			unsubscribeFrom(event: string, cb?: (...args: any[]) => any): void;
		}
	}

	interface Array<T> {
		mapAsync<R>(callbackFn: (value: T, index: number, array: T[]) => R | PromiseLike<R>, thisArg?: any): Promise<R[]>;
		first(): T;
		last(): T;
		equals(array: any[]): boolean;
	}

	interface Math {
		round(x: number, digits?: number): number;
		ceil(x: number, digits?: number): number;
		floor(x: number, digits?: number): number;
		toDegrees(alpha: number): number;
		toRadians(alpha: number): number;
	}
}
