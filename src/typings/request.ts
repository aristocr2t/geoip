import { FastifyReply as iFastifyReply, FastifyRequest as iFastifyRequest } from 'fastify';
import { Http2ServerResponse } from 'http2';

export type RequestHeaders = {
	authorization?: string;
	[key: string]: string | undefined;
};

export interface CreateData<T = {}> {
	item: T;
}

export interface UpdateData<T = {}> {
	item: T;
}

declare global {
	interface FastifyRequest<T = any> extends iFastifyRequest {
		user: T;
	}
	interface FastifyReply extends iFastifyReply<Http2ServerResponse> {}
}
