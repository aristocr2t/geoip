export type ConfigInterface = typeof import('../configs/default.json');
export type PostgresConfig = ConfigInterface['postgres'];
export type RabbitmqConfig = ConfigInterface['rabbitmq'];
