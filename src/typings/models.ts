export type ObjectId = string;
export type Coords = [number, number];
export type ListResult<T> = [number, T[]];
