export interface ResponseData<T = {}> extends EmptyResponseData {
	item: T;
}

export interface ListResponseData<T = {}> extends EmptyResponseData {
	count: number;
	items: T[];
}

export interface EmptyResponseData {
	statusCode?: number;
	message?: string;
}
