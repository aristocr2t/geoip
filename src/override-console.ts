import chalk from 'chalk';
import cluster from 'cluster';
import moment from 'moment';
import Valera, { Caller, Level, Record } from 'valera';
import { config } from 'bluebird';
import { resolve as pathResolve } from 'path';
import { inspect } from 'util';
import { createWriteStream, existsSync, mkdirSync } from 'fs';

const rootPath = pathResolve(__dirname, '..');
const logsDir = pathResolve(rootPath, 'logs');
const accessLogFile = pathResolve(logsDir, 'access.log');
const errorLogFile = pathResolve(logsDir, 'error.log');
if (!existsSync(logsDir)) {
	mkdirSync(logsDir, { recursive: true });
}

const accessLogFileStream = createWriteStream(accessLogFile, { flags: 'a' });
const errorLogFileStream = createWriteStream(errorLogFile, { flags: 'a' });
const LOG_TYPE_COLORS = {
	trace: chalk.blue,
	verbose: chalk.magenta,
	debug: chalk.cyan,
	info: chalk.green,
	warn: chalk.yellow,
	error: chalk.red,
	critical: chalk.red,
};
const widpid = `[${cluster.isMaster ? '0' : cluster.worker.id}:${process.pid}]`;

const errorLevels: Level[] = ['warn', 'error', 'critical'];

Valera.configure({
	metadata: { wid: cluster.isMaster ? '0' : cluster.worker.id, pid: process.pid },
	formats: [
		`${chalk.cyan(`{% pipes.date(date) %}`)} ${chalk.white(
			widpid,
		)} {% pipes.level(level) %}{% pipes.nameColored(name) %} {% pipes.messageColored(args) %}<-|->${chalk.gray(
			'{% pipes.fileTerminal(caller) %}',
		)}`,
		`{% pipes.date(date) %} ${widpid} {% level.toUpperCase() %}{% pipes.name(name) %} {% pipes.file(caller) %} {% pipes.message(args) %}`,
	],
	pipes: {
		date(v: number): string {
			return moment(v).format('YYYY-MM-DD HH:mm:ss.SSS');
		},
		nameColored(v: string | undefined): string {
			return v ? ' ' + chalk.magentaBright(`<${v}>`) : '';
		},
		name(v: string | undefined): string {
			return v ? ` <${v}>` : '';
		},
		level<V extends keyof typeof LOG_TYPE_COLORS>(v: V): string {
			return LOG_TYPE_COLORS[v].call(chalk, (v as any).toUpperCase());
		},
		messageColored(args: any[]): string {
			return args.map(x => (typeof x === 'string' ? x : x instanceof Error ? x.stack : inspect(x, false, null, true))).join('\n');
		},
		message(args: any[]): string {
			return args.map(x => (typeof x === 'string' ? x : x instanceof Error ? x.stack : inspect(x, false, null, false))).join('\n');
		},
		fileTerminal({ fileName, line, column }: Caller): string {
			return `${fileName.startsWith(rootPath) ? `.${fileName.substring(rootPath.length)}` : fileName}:${line}:${column}`;
		},
		file({ fileName, line, column }: Caller): string {
			return `${fileName}:${line}:${column}`;
		},
	},
	handler(record: Record): void {
		const [consoleMessage, fileMessage] = record.messages();
		if (!errorLevels.includes(record.level)) {
			process.stdout.write(consoleMessage + '\n');
			accessLogFileStream.write(fileMessage + '\n');
		} else {
			process.stderr.write(consoleMessage + '\n');
			errorLogFileStream.write(fileMessage + '\n');
		}
	},
});

Valera.overrideConsole();

config({
	warnings: {
		wForgottenReturn: false,
	},
});

process.on('unhandledRejection', (reason, promise) => {
	promise.catch(err => {
		throw err;
	});
	throw reason;
});
