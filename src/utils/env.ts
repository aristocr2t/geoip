import fs from 'fs';

export function getEnv(filename: string): { [key: string]: string } {
	return fs
		.readFileSync(filename, { encoding: 'utf8' })
		.split(/\r?\n/)
		.filter(v => /^[\w\d]+=/.test(v))
		.map(s => {
			const [, k, v] = Array.from(s.match(/^([\w\d]+)=(.*)/) ?? []);
			return k ? { [k]: v } : null;
		})
		.filter((x: any): x is { [key: string]: string } => x !== null)
		.reduce((p, n) => Object.assign(p, n), {});
}
