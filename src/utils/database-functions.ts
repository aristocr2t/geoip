import { col, fn, literal } from 'sequelize';
import { inspect, isString, isUndefined } from 'util';

import { PostgresConfig, RabbitmqConfig } from '../typings';

const isProd = process.env.NODE_ENV === 'production';
const LIMIT = isProd ? 10 : undefined;

export function makeDbUrl(data: PostgresConfig | RabbitmqConfig): string {
	return (
		`${data.dialect}://${data.username}:${data.password}@${data.host}:${data.port}/` +
		`${'database' in data ? data.database : 'vhost' in data ? data.vhost : ''}`.replace(/^\/+/, '')
	);
}

export function escapeString(str: string): string {
	return isString(str) ? str.replace(/'/g, `''`).replace(/\0/g, '\\0') : '';
}

export function getChangedValues<T extends {}>(data: T, item: any): Partial<T> {
	const values: any = {};
	for (const k in data) {
		if (data.hasOwnProperty(k)) {
			if (data[k] !== item[k] && !isUndefined(item[k])) {
				values[k] = data[k];
			}
		}
	}
	return values;
}

export function pagination<P extends {}>(
	params: P,
	{
		pageSize,
		pageIndex,
	}: {
		pageIndex: number;
		pageSize: number;
	},
): P & {
	limit?: number;
	offset: number;
} {
	return Object.assign(
		{
			limit: pageSize > -1 ? pageSize : LIMIT,
			offset: pageSize > -1 ? pageIndex * pageSize : 0,
		},
		params,
	);
}

export function hasValues(values: object): boolean {
	return !!(values || Object.keys(values).length);
}

export function distanceFn([lat, long]: [number, number], tableName?: string) {
	return fn('get_distance', col(tableName ? `${tableName}.coords` : 'coords'), lat, long);
}

export function distanceCol(coords: [number, number], tableName?: string) {
	return [distanceFn(coords, tableName), 'distance'];
}

export function similarityFn(searchText: string, cols: string) {
	return fn('word_similarity', searchText, literal(cols));
}

export function similarityCol(searchText: string, cols: string) {
	return [similarityFn(searchText, cols), 'similarity'];
}

export type SqlValue = number | boolean | string | Buffer | null | Date | SqlValue[];

export function formatSql(sql: string, values: { [key: string]: SqlValue }): string {
	return sql.replace(/:+(?!\d)(\w+)/g, (value, key) => {
		if ('::' === value.slice(0, 2)) {
			return value;
		}
		if (values[key] !== undefined) {
			return escape(values[key]);
		}
		throw new Error(`Named parameter "${value}" has no value in the given object.`);
	});
}

function arrayToList(array: SqlValue[]): string {
	return array.reduce((sql: string, val: SqlValue, i: number) => {
		if (i !== 0) {
			sql += ', ';
		}
		if (Array.isArray(val)) {
			sql += `(${arrayToList(val)})`;
		} else {
			sql += escape(val);
		}
		return sql;
	}, '');
}

function escape(val: SqlValue): string {
	if (val === null) {
		return 'NULL';
	}

	switch (typeof val) {
		case 'boolean':
			return (!!val).toString();
		case 'number':
			return val.toString();
	}

	if (val instanceof Date) {
		return `'${val.toISOString()}'`;
	}

	if (Buffer.isBuffer(val)) {
		if (Array.isArray(val)) {
			val = Buffer.from(val);
		} else {
			val = Buffer.from(val.toString());
		}
		const hex = `X'${val.toString('hex')}'`;
		return hex;
	}

	if (Array.isArray(val)) {
		return arrayToList(val);
	}

	if (!val.replace) {
		throw new Error(`Invalid value ${inspect(val)}`);
	}

	val = val.replace(/'/g, `''`);
	val = val.replace(/\0/g, '\\0');
	return `'${val}'`;
}
