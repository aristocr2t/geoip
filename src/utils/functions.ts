import XRegExp from 'xregexp';
import { isNumber, isString } from 'util';

import { ObjectId } from '../typings';

export const NIL_UUID = '00000000-0000-0000-0000-000000000000';
export const VUUID = '([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})';
export const PHONE_MASK = /^\+\d{11,13}$/;
export const EMAIL_MASK = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
export const UUID_MASK = /^[a-f0-9]{8}-[a-f0-9]{4}-(4[a-f0-9]{3}|0000)-([ab89][a-f0-9]{3}|0000)-[a-f0-9]{12}$/;

export const isId = (x: any): x is ObjectId => UUID_MASK.test(x);
export const isIds = (x: any): x is ObjectId[] => Array.isArray(x) && x.every(id => isId(id));
export const isCoords = (x: any): x is [number, number] => Array.isArray(x) && isNumber(x[0]) && isNumber(x[1]);
export const isPhone = (x: any): x is string => PHONE_MASK.test(x);
export const isEmail = (x: any): x is string => EMAIL_MASK.test(x);

export const bind2 = <T extends {}>(o: T, fn: (...args: any[]) => any): any => fn.bind(o);

export const genHashtags = (text: string): string[] => (isString(text) ? text.match(XRegExp('(?:^|\\s)(#[\\p{L}\\d_]+)', 'g')) || [] : []);
