import chalk from 'chalk';
import SocketIO from 'socket.io';
import { isArray } from 'util';

export namespace WebSocketCluster {
	export class Socket {
		private readonly eventCallbacks: { [event: string]: Array<(...args: any[]) => any> } = {};
		private readonly cluster: Server;
		private socketOnEventHandler!: (data: { id: string; pid: number; event: string; args: any[] }) => any;
		readonly id: string;
		readonly pid: number;
		readonly isOwn: boolean;

		constructor(setup: { server: Server; id: string; pid: number }) {
			this.cluster = setup.server;
			this.id = setup.id;
			this.pid = setup.pid;
			this.isOwn = this.pid === process.pid;
			this.onInit();
		}

		private onInit(): void {
			if (process.pid !== this.pid) {
				this.socketOnEventHandler = (data: { id: string; pid: number; event: string; args: any[] }) => {
					if (this.id === data.id && isArray(this.eventCallbacks[data.event])) {
						for (let i = 0, length = this.eventCallbacks[data.event].length; i < length; i++) {
							const val = this.eventCallbacks[data.event][i](...data.args);
							if (val instanceof Promise) {
								val.catch(err => {
									throw err;
								});
							}
						}
					}
				};
				process.subscribeTo('SOCKET_ON', this.socketOnEventHandler);
			}
		}

		destroy(): void {
			if (this.socketOnEventHandler) {
				process.unsubscribeFrom('SOCKET_ON', this.socketOnEventHandler);
			}
		}

		emit(event: string, ...args: any[]): void {
			if (process.pid === this.pid) {
				if (this.cluster.server.sockets.connected[this.id]) {
					this.cluster.server.sockets.connected[this.id].emit(event, ...args);
				}
			} else {
				process.emitTo('SOCKET_EMIT', { id: this.id, pid: this.pid, event, args });
			}
		}

		on(event: string, cb: (...args: any[]) => any): void {
			if (this.cluster.sockets[this.id]) {
				if (!this.eventCallbacks[event]) {
					this.eventCallbacks[event] = [cb];
					if (process.pid === this.pid) {
						this.cluster.server.sockets.connected[this.id].on(event, (...args) => {
							const startAt = process.hrtime();
							process.emitTo('SOCKET_ON', { id: this.id, pid: this.pid, event, args });
							Promise.all(this.eventCallbacks[event].map(cb => cb(...args)).filter(val => val instanceof Promise)).catch(err => {
								throw err;
							});
							const diff = process.hrtime(startAt);
							const time = diff[0] * 1e3 + diff[1] * 1e-6;
							console.info(
								`${chalk.green('SOCKET')} ${this.cluster.server.path()}/${event} ${chalk.yellow(`${time}ms`)} ${chalk.magenta(this.id)}`,
							);
						});
					}
				} else {
					this.eventCallbacks[event].push(cb);
				}
			}
		}
	}

	export class Server {
		private readonly connectEventCallbacks: Array<(socket: WebSocketCluster.Socket) => any> = [];
		private readonly disconnectEventCallbacks: Array<(socket: Socket) => any> = [];
		readonly sockets: { [id: string]: Socket } = {};

		constructor(readonly server: SocketIO.Server) {
			this.onInit();
		}

		private onInit(): void {
			process.subscribeTo('SOCKET_CONNECT', (data: { id: string; pid: number }) => {
				this.sockets[data.id] = new Socket({ server: this, id: data.id, pid: data.pid });
				for (let i = 0, length = this.connectEventCallbacks.length; i < length; i++) {
					this.connectEventCallbacks[i](this.sockets[data.id]);
				}
			});
			process.subscribeTo('SOCKET_DISCONNECT', (data: { id: string; pid: number }) => {
				for (let i = 0, length = this.disconnectEventCallbacks.length; i < length; i++) {
					this.disconnectEventCallbacks[i](this.sockets[data.id]);
				}
				if (this.sockets[data.id]) {
					this.sockets[data.id].destroy();
				}
				delete this.sockets[data.id];
			});
			process.subscribeTo('SOCKET_EMIT', (data: { id: string; pid: number; event: string; args: any[] }) => {
				if (data.pid === process.pid && this.server.sockets.connected[data.id]) {
					this.server.sockets.connected[data.id].emit(data.event, ...data.args);
				}
			});
			this.server.on('connection', socket => {
				process.emitTo('SOCKET_CONNECT', { id: socket.id, pid: process.pid });
				this.sockets[socket.id] = new Socket({ server: this, id: socket.id, pid: process.pid });
				socket.on('disconnect', () => {
					process.emitTo('SOCKET_DISCONNECT', { id: socket.id, pid: process.pid });
					for (let i = 0, length = this.disconnectEventCallbacks.length; i < length; i++) {
						this.disconnectEventCallbacks[i](this.sockets[socket.id]);
					}
					if (this.sockets[socket.id]) {
						this.sockets[socket.id].destroy();
					}
					delete this.sockets[socket.id];
				});
				for (let i = 0, length = this.connectEventCallbacks.length; i < length; i++) {
					this.connectEventCallbacks[i](this.sockets[socket.id]);
				}
			});
		}

		handleConnect(cb: (socket: Socket) => any): void {
			this.connectEventCallbacks.push(cb);
		}

		handleDisconnect(cb: (socket: Socket) => any): void {
			this.disconnectEventCallbacks.push(cb);
		}
	}
}
