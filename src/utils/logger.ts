import chalk from 'chalk';
import responseTime from 'response-time';
import Valera from 'valera';
import { QueueWorker } from 'queue-worker';

export const queueLogger = new Valera({ name: 'queue' });
QueueWorker.ERROR_HANDLER = queueLogger.error.bind(queueLogger);
export const requestLogger = new Valera({ name: 'request' });
export const logger = new Valera({ name: 'application' });
export const geoLogger = new Valera({ name: 'geo' });
export const pushLogger = new Valera({ name: 'push' });
export const s3Logger = new Valera({ name: 's3' });
export const mediaLogger = new Valera({ name: 'media' });

export const reqLogger = responseTime((req: any, res: any, time: number) => {
	requestLogger.info(
		[
			chalk.green(req.method),
			chalk.yellow(res.statusCode),
			req.url,
			chalk.yellow(time.toFixed(0) + 'ms'),
			chalk.green(`${req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.ip}`),
			chalk.magenta(req.headers['user-agent']),
		].join(' '),
	);
});
