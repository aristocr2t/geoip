import '../typings';

import ejs from 'ejs';

ejs.openDelimiter = '{';
ejs.closeDelimiter = '}';

Array.prototype.mapAsync = function<T, R>(
	callbackFn: (value: T, index: number, array: T[]) => R | PromiseLike<R>,
	thisArg?: any,
): Promise<R[]> {
	return Promise.all<R>((this as T[]).map<R | PromiseLike<R>>(callbackFn, thisArg));
};

Array.prototype.first = function<T>(): T | null {
	return this.length > 0 ? this[0] : null;
};

Array.prototype.last = function<T>(): T | null {
	return this.length > 0 ? this[this.length - 1] : null;
};

Array.prototype.equals = function(array: any[]): boolean {
	if (!array || this.length !== array.length) {
		return false;
	}

	for (let i = 0, l = this.length; i < l; i++) {
		if (this[i] !== array[i] || (this[i] instanceof Array && array[i] instanceof Array && !this[i].equals(array[i]))) {
			return false;
		}
	}

	return true;
};

Object.defineProperty(Array.prototype, 'equals', { enumerable: false });
Object.defineProperty(Array.prototype, 'last', { enumerable: false });
Object.defineProperty(Array.prototype, 'first', { enumerable: false });
Object.defineProperty(Array.prototype, 'mapAsync', { enumerable: false });

const MathRound = Math.round;
const MathCeil = Math.ceil;
const MathFloor = Math.floor;
const $180pi = 180 / Math.PI;
const $pi180 = Math.PI / 180;

Math.round = function(x: number, digits: number = 0): number {
	if (digits > 0) {
		digits = 10 ** MathRound.call(this, digits);
		return MathRound.call(this, x * digits) / digits;
	}
	return MathRound.call(this, x);
};

Math.ceil = function(x: number, digits: number = 0): number {
	if (digits > 0) {
		digits = 10 ** MathRound.call(this, digits);
		return MathCeil.call(this, x * digits) / digits;
	}
	return MathCeil.call(this, x);
};

Math.floor = function(x: number, digits: number = 0): number {
	if (digits > 0) {
		digits = 10 ** MathRound.call(this, digits);
		return MathFloor.call(this, x * digits) / digits;
	}
	return MathFloor.call(this, x);
};

Math.toDegrees = function(alpha: number): number {
	return alpha * $180pi;
};

Math.toRadians = function(alpha: number): number {
	return alpha * $pi180;
};
