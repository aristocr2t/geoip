import sqlf from 'sql-formatter';
import { Op } from 'sequelize';
import { Sequelize } from 'sequelize-typescript';

import config from './config';
import { makeDbUrl } from './utils';

const queryFn = Sequelize.prototype.query;
// @ts-ignore
Sequelize.prototype.query = function(): Promise<any> {
	return (
		queryFn
			// @ts-ignore
			.apply(this, arguments)
			.catch((err: Error & { parent: Error & { detail: string } }) => {
				if (err.parent?.detail) {
					err.message += `: ${err.parent.detail}`;
				}

				throw err;
			})
	);
};

const operatorsAliases = {
	$eq: Op.eq,
	$ne: Op.ne,
	$gte: Op.gte,
	$gt: Op.gt,
	$lte: Op.lte,
	$lt: Op.lt,
	$not: Op.not,
	$in: Op.in,
	$notIn: Op.notIn,
	$is: Op.is,
	$like: Op.like,
	$notLike: Op.notLike,
	$iLike: Op.iLike,
	$notILike: Op.notILike,
	$regexp: Op.regexp,
	$notRegexp: Op.notRegexp,
	$iRegexp: Op.iRegexp,
	$notIRegexp: Op.notIRegexp,
	$between: Op.between,
	$notBetween: Op.notBetween,
	$overlap: Op.overlap,
	$contains: Op.contains,
	$contained: Op.contained,
	$adjacent: Op.adjacent,
	$strictLeft: Op.strictLeft,
	$strictRight: Op.strictRight,
	$noExtendRight: Op.noExtendRight,
	$noExtendLeft: Op.noExtendLeft,
	$and: Op.and,
	$or: Op.or,
	$any: Op.any,
	$all: Op.all,
	$values: Op.values,
	$col: Op.col,
};

const sequelize = new Sequelize(makeDbUrl(config.postgres), {
	logging(sql: string, info: any) {
		if (info.type !== 'RAW') {
			return;
		}

		console.info('\n', sqlf.format(sql.replace(/^Executing \(default\): /, '')));
	},
	define: {
		charset: 'utf8',
		freezeTableName: true,
		paranoid: true,
		timestamps: true,
		underscored: true,
		createdAt: 'createdAt',
		updatedAt: 'updatedAt',
		deletedAt: 'deletedAt',
	},
	operatorsAliases,
});

export default sequelize;

export const databaseSync: Promise<any> = Promise.resolve()
	.then(() => import('./models'))
	.then(models => {
		sequelize.addModels([
		]);
	})
	.catch(err => console.error(err));
