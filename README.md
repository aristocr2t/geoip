# @test/server

## Scripts

### development

- `npm run format` – run prettier
- `npm run dev` – run server in development mode
- `npm run lint` – run tslint
- `npm run test:e2e` – run e2e tests

- `npm run migrate` – migrate in development mode
- `npm run migrate:undo` – undo migrate in development mode
- `npm run seed` – seed in development mode
- `npm run seed:undo` – undo seed in development mode

### production

- `npm run build` – build project
- `npm run start` – run server

- `npm run migrate:prod` – migrate
- `npm run migrate:undo:prod` – undo migate
- `npm run seed:prod` – seed
- `npm run seed:undo:prod` – undo seed
