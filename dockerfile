FROM node:lts-buster

RUN apt-get update >/dev/null;
RUN apt-get install ffmpeg imagemagick graphicsmagick -y -qq >/dev/null;

RUN mkdir -p /app
WORKDIR /app

COPY . .

RUN npm install --loglevel=error
RUN npm run build

EXPOSE 3000

CMD npm run start
